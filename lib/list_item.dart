import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo_listview_flipcard/data.dart';

class ListItem extends StatelessWidget {
  const ListItem({Key key, this.data, this.index, this.toggleCard})
      : super(key: key);

  final Data data;
  final int index;
  final Function toggleCard;

  @override
  Widget build(BuildContext context) {
    return (index == 0)
        ? renderFirstItem(context)
        : renderNonFirstItem(context);
  }

  Widget renderFirstItem(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: () => toggleCard(),
        child: Stack(
          children: <Widget>[
            Container(
              height: 150,
              width: MediaQuery.of(context).size.width,
              child: Material(
                color: Color(data.color),
                elevation: 6.0,
                borderRadius:
                    BorderRadius.only(bottomLeft: Radius.circular(70)),
                shadowColor: Colors.blue,
                child: Container(
                  margin: EdgeInsets.only(top: 20, left: 50, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border:
                                    Border.all(color: Colors.white, width: 1)),
                            child: Icon(
                              Icons.laptop_chromebook,
                              color: Colors.white,
                              size: 40,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Education',
                              style: TextStyle(
                                  color: Colors.white, letterSpacing: 2),
                            ),
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                    color:
                                        Color(Data.dataList[index + 1].color),
                                    width: 1)),
                            child: Icon(
                              Icons.headset,
                              color: Color(Data.dataList[index + 1].color),
                              size: 40,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8),
                            child: Text(
                              'MUSIC',
                              style: TextStyle(
                                  color: Colors.white, letterSpacing: 2),
                            ),
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border:
                                    Border.all(color: Colors.white, width: 1)),
                            child: Icon(
                              Icons.beach_access,
                              color: Colors.white,
                              size: 40,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8),
                            child: Text(
                              'SPORT',
                              style: TextStyle(
                                  color: Colors.white, letterSpacing: 2),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget renderNonFirstItem(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: () => toggleCard(),
        child: Stack(
          children: <Widget>[
            Container(
              height: 200,
              width: MediaQuery.of(context).size.width,
              child: Material(
                color: Color(data.color),
                elevation: 6.0,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(70),
                ),
                shadowColor: Colors.blue,
                child: Container(
                  margin: EdgeInsets.only(top: 20, left: 50),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Text(
                          data.date,
                          style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Text(
                          data.title,
                          style: TextStyle(fontSize: 24, color: Colors.white),
                        ),
                      ),
                      Container(
                        width: 100,
                        padding: const EdgeInsets.all(10),
                        child: Stack(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: Colors.white, width: 1)),
                              child: Icon(
                                Icons.supervised_user_circle,
                                color: Colors.white,
                              ),
                            ),
                            Positioned(
                                left: 20,
                                child: Container(
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                          color: Colors.white, width: 1)),
                                  child: Icon(
                                    Icons.supervised_user_circle,
                                    color: Colors.white,
                                  ),
                                ))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
