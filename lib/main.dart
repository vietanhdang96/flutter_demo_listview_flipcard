import 'package:flutter/material.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter_demo_listview_flipcard/data.dart';
import 'package:flutter_demo_listview_flipcard/list_item.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  GlobalKey<FlipCardState> cardKey = GlobalKey<FlipCardState>();

  @override
  Widget build(BuildContext context) {
    return FlipCard(
      key: cardKey,
      flipOnTouch: false,
      front: Scaffold(
        body: InkWell(
          child: Container(
            child: ListView.builder(
                itemCount: Data.dataList.length,
                itemBuilder: (context, index) {
                  Data data = Data.dataList[index];
                  return ListItem(
                    data: data,
                    index: index,
                    toggleCard: toggleCard,
                  );
                }),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          backgroundColor: Colors.white,
          child: Icon(
            Icons.add,
            color: Colors.black,
          ),
        ),
      ),
      back: Scaffold(
        backgroundColor: Colors.blue,
        body: InkWell(
          onTap: toggleCard,
          child: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Best of Luck',
                    style: TextStyle(
                        fontSize: 35,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'vietanhdang96',
                    style: TextStyle(
                        fontSize: 25,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                  Icon(
                    Icons.favorite,
                    color: Colors.white,
                    size: 60,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void toggleCard() {
    cardKey.currentState.toggleCard();
  }
}
