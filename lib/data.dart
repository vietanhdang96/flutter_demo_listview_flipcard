class Data{
  String date;
  String title;
  int color;

  Data({this.date, this.title, this.color});

  static List<Data> dataList=[
    Data(
      date: "TODAY 9:30 PM",
      title: "Header",
      color: 0xFF000000,
    ),
    Data(
      date: "TODAY 8:30 PM",
      title: "Android",
      color: 0xFF52b5f0,
    ),
    Data(
      date: "TUESDAY 5:30 PM",
      title: "Kotlin",
      color: 0xFF92b5f0,
    ),
    Data(
      date: "THURDAY 6:30 AM",
      title: "Java",
      color: 0xFF6f6eee,
    ),
    Data(
      date: "FRIDAY 4:30 PM",
      title: "Flutter",
      color: 0xFF913bfc,
    ),
  ];
}